Ext.define('Vmoss.model.Menu', {
    extend:'Addition.lib.CModel',

    fields:[
        {name:'text', type:'string'},
        {name:'singleClickExpand', type:'bool'},
        {name:'leaf', type:'bool', defaultValue:false},

        {name:'model', type:'string'}
    ]
});
