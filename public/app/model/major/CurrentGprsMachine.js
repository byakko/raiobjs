Ext.define('Vmoss.model.major.CurrentGprsMachine', {
    extend:'Addition.lib.CModel',

    fields:[
        'id',
        'vm_cd'
    ],

    proxy:{
        type:'rest',
        url:'/objects/current_gprs_machines'
    }
});