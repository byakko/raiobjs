Ext.define('Vmoss.model.major.Uriagefull', {
    extend:'Addition.lib.CModel',

    fields:[
        'id',
        'denpyo_no',
        {
            name: 'work_dtm',
            type: 'date',
            dateFormat: 'YmdHis'
        },
        'basyo_name',
        'salesnumber',
        'amount',
        {
            name: 'patrol',
            type: 'boolean'
        }
    ],

    proxy:{
        type:'rest',
        url:'/objects/uriagefull_rs'
    }
});