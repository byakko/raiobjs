Ext.define('Vmoss.model.major.FlowSample.Struct', {
    extend:'Addition.lib.CModel',

    fields:[
        'id',
        {
        	name: 'source',
        	convert: function(value, record) {
                var obj = (value) ?  Ext.JSON.decode(value) : '';

                return obj;
            }
        }
    ],

    proxy:{
        type:'rest',
        url:'/objects/architecture_nodes',
        reader: {
            root:'root',
            totalProperty:'totalLength'
        }
    }
});