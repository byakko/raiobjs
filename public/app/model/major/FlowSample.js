Ext.define('Vmoss.model.major.FlowSample', {
	requires: [
		'Vmoss.model.major.FlowSample.Struct'
	],
    extend:'Addition.lib.CModel',

    associations: [
    	{ 
    		type: 'belongsTo', 
    		model: 'Vmoss.model.major.FlowSample.Struct',
	    	getterName: 'getStruct',
	    	primaryKey: 'id',
            foreignKey: 'machine_kind'
    	}
    ],

    fields:[
	    'id',
        'code',
        {
        	name: 'machine_kind',
        	type: 'int'
        }
    ],

    proxy: {
        type: 'localstorage',
        id: 'flow-sample'
    }
});