Ext.define('Vmoss.model.major.CurrentGprsStorage', {
    requires: ['Vmoss.model.major.CurrentGprsMachine'],
    extend:'Addition.lib.CModel',

    associations:[
        { type:'belongsTo', model:'Vmoss.model.major.CurrentGprsMachine', foreignKey:'vm_id'}
    ],

    fields:[
        'id',
        'column_no',
        'remained',
        'vm_id',
        'gprsterminal_cd',
        'basyo_name',
        'vm_cd',
        'fullsyohinsu'
    ],

    proxy:{
        type:'rest',
        url:'/objects/current_gprs_storages'
    },

    fieldsExtend:[
        {field: 'column_no', label: '货道'},
        {field: 'remained', label: '余货数'},
        {field: 'vm_id', label: '自售机'},
        {field: 'gprsterminal_cd', label: 'GPRS Code'},
        {field: 'basyo_name', label: '点位名'},
        {field: 'vm_cd', label: '自售机'},
        {field: 'fullsyohinsu', label: '满仓数'}
    ]
});