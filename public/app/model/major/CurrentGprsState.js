Ext.define('Vmoss.model.major.CurrentGprsState', {
    extend:'Addition.lib.CModel',

    fields:[
        'id',
        'gprsterminal_cd',
        'vm_cd',
        'basyo_name',
        'trouble',
        'no_change',
        'lack_warning',
        'early_warning',
        'state'
    ],

    proxy:{
        type:'rest',
        url:'/objects/current_gprs_states'
    },

    fieldsExtend:[
        {field: 'gprsterminal_cd', label: 'GPRS Code'},
        {field: 'basyo_name', label: '点位名'},
        {field: 'vm_cd', label: '自售机'},
        {field: 'trouble', label: '故障'},
        {field: 'no_change', label: '无零钱警报'},
        {field: 'lack_warning', label: '缺货警报'},
        {field: 'early_warning', label: '缺货预警'},
        {field: 'state', label: '当前状态'}
    ]
});