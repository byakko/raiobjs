Ext.define('Vmoss.model.major.Role', {
    extend:'Addition.lib.CModel',

    fields:[
        'id',
        'user_id',
        'role_name'
    ],

    proxy:{
        type:'rest',
        url:'/objects/roles'
    }
});