Ext.define('Vmoss.model.CurrentUser', {
    extend:'Addition.lib.CModel',

    fields:[
        {name:'userCd', type:'string'},
        {name:'name', type:'string'},
        {name:'role', type:'auto'},
        {name:'bumon', type:'auto'},
        {name:'password', type:'string'},
        {name:'alterPassword', type:'string'},
        {name:'confirmPassword', type:'string'}
    ],

    validations:[
        {type:'presence', field:'password'},
        {type:'length', field:'modifyPassword', min:6}
    ],

    save: function (options) {
        Ext.Ajax.request(Ext.merge({
            url: '/services/main/actions/alter_password/invoke',
            method: 'POST'
        }, options));
    },

    statics:{
        load:function () {
            var user = Ext.create('Vmoss.model.CurrentUser');
            Ext.Ajax.request({
                method: 'GET',
                url:'/services/main/actions/ext_current_user/invoke',
                success:function (response, options) {
                    var obj = Ext.decode(response.responseText);
                    for (var attr in obj) {
                        user.set(attr, obj[attr])
                    }
                }
            });
            return user;
        }
    }
});