Ext.define('Vmoss.controller.Vmoss', {
    requires: [
        'Ext.History',
        'Ext.state.Provider',
        'Ext.state.LocalStorageProvider'
    ],
    extend: 'Ext.app.Controller',

    refs: [
        {ref: 'headerView', selector: 'headerview'},
        {ref: 'mainView', selector: 'mainview'},
        {ref: 'menuView', selector: 'menuview'}
    ],

    serverExceptionMsg: 'Server Expection',

    // this.getMainView().openPanel(instance.raw);
    ROUTER_CONFIG: {
        '!': {
            method: 'getMainView',
            children: {
                'tab': {
                    method: 'openPanel'
                }
            }
        }
    },
    
    init: function() {
        var me = this;

        me.stateInit();
        me.ajaxExtend();
    },

    stateInit: function(){
        Ext.state.Manager.setProvider(new Ext.state.LocalStorageProvider());
    },

    onLaunch: function() {
        this.historyInit();
    },

    historyInit: function(){
        var me = this;

        Ext.History.init(function(history, options){
            me.route(history.currentToken);
            history.on({
                change: function(token, options){
                    me.route(token);
                }
            });
        });
    }, 

    ajaxExtend: function(){
        var me = this;

        Ext.Ajax.on({
            // 记录本程序向服务器发起的所有请求
            beforerequest:function (conn, options) {
                if (Addition.Tool.requestLog){
                    Addition.Tool.log([conn, 'Requesting']);
                }
                if(options.method !== 'GET' && options.method !== 'get'){
                    options.params = options.params || {};
                    Ext.mergeIf(options.params, Addition.Tool.csrfToken);
                }
            },
            // 此处处理服务器所捕获的逻辑异常
            requestcomplete:function (conn, response, options) {
                var operation = options.operation,
                    responseObj = Ext.JSON.decode(response.responseText);
                if (responseObj.success) return;
                // 默认行为将会丢弃返回数据, 此处以response属性来存储
                Ext.apply(operation, {
                    response: response
                });
                if (responseObj.exceptionType) {
                    Addition.Tool.promptBox(responseObj.exceptionType, responseObj.exceptionMessage);
                }
            },
            // 此处为默认的异常提示 404/500
            requestexception:function (conn, response, options) {
                Addition.Tool.promptBox(me.serverExceptionMsg, response.statusText);
            }
        });
    },

    route: function(token){
        var me = this,
            medium = me,
            paths = (token ? token.split('/') : []),
            path,
            config = me.ROUTER_CONFIG;

        while (paths.length > 0) {
            path = paths.shift();
            if (config[path] !== undefined) {
                if (config[path]['children']) {
                    medium = medium[config[path]['method']]();
                    config = config[path]['children'];
                } 
                else {
                    medium[config[path]['method']](paths.join('/'));
                    break;
                }
            }
            else {
                break;
            }
        }
    }
});