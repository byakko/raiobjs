Ext.onReady(function() {
    source = {
    	'Vmoss.controller.Vmoss': {
    		serverExceptionMsg: '服务器异常'
    	},
    	'Vmoss.view.main.button.GridExport': {
    		text: "导出",
	        tooltipText: "导出符合检索条件的",
	        confirmText: "您确定要导出吗?"
    	},
    	'Vmoss.view.menu.View': {
    		title: '导航菜单'
    	},
    	'Addition.lib.CForm': {
    		remoteExceptionTitle: '保存出错', 
    		remoteExceptionMsg: '项目输入错误, 请依提示输入正确内容'
    	},
    	'Vmoss.view.header.UserMenu': {	
    		modifyPwd: '修改密码',
    		logout: '退出'
		},
		'Vmoss.view.header.AlterpwForm': {
			okText: '确定',
	    	cancelText: '取消',
	    	notEqual: '修改密码与确认密码不一致!',
            successTitle: '修改成功',
            successText: '密码修改成功!',
            modelField: [
                {
                    label: '初始密码', 
                    blankText: '请填写初始密码！'
                },
                {
                    label: '修改密码', 
                    blankText: '请填写修改密码！'
                },
                { 
                    label: '确认密码', 
                    blankText: '请填写确认密码！'
                }
            ]
		},
		'Vmoss.view.header.AlterpwView': {
			title: "修改密码"
		},
		'Vmoss.view.main.Standard': {
			searchFormTitle: '检索条件'
		}
    };

    Addition.Tool.lang = 'zh_CN';
    Addition.Tool.appTranslate(source);
});