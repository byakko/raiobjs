Ext.define('Vmoss.view.main.nav.Flow', {
    extend: 'Ext.container.Container',

    layout: {
    	type: 'table',
    	columns: 16
    },

    defaults: {
    	width: 80, 
    	height: 70
    },
	

    initComponent: function () {
    	var me = this;

    	Ext.apply(me, {
    		items: [
    			{
	    			title:'Item 1'
		        },{
		            title:'Item 2'
		        },{
		            title:'Item 3'
		        },{
		            title:'Item 3'
		        },{
		            title:'Item 3'
		        },{
		            title:'Item 3'
		        }
    		]
    	});

    	me.callParent(arguments);
    }
});