/**
 * ExtendConfig
 *  scaffold:Object
 *
 *  featureList:Object
 *  searchFeature:Array
 *  addFeature:Array
 *  benchFeature:Array...
 */
Ext.define('Vmoss.view.main.Standard', {
    requires: [
        'Vmoss.view.main.SearchForm',
        'Vmoss.view.main.Grid',
        'Addition.lib.CCombo'
    ],
    extend: 'Vmoss.view.main.Basic',

    searchFormTitle: 'Search',

    layout:{
        type:'vbox',
        align:'stretch'
    },

    isStandard: true,

    buttonList: ['gridadd', 'gridmodify', 'griddestroy'],

    featureKeys: ['add', 'modify', 'bench', 'search', 'grid'],
    extendKeys: ['fields'],

    initComponent:function () {
        var me = this,
            modelName = (me.modelName || me.scaffold.split("-")[0]),
            instance = Ext.create('Vmoss.model.major.' + modelName),
            featureList = me.featureCollect(),
            searchItem = Ext.create('Ext.form.FieldSet', {
                layout: 'fit',
                title: me.searchFormTitle,
                collapsible: true,
                collapsed: true,
                items:[
                    Ext.create('Vmoss.view.main.SearchForm', {
                        bind:instance,
                        modelField: featureList.searchFeature
                    })
                ]
            }),
            mainGrid = Ext.create('Vmoss.view.main.Grid', {
                instanceLabel: me.instanceLabel,
                searchInstance: instance,
                model: modelName,
                featureList: featureList,
                buttonList: me.buttonList,
                gridConfig: me.gridConfig
            });

        Ext.apply(me, {
            items:[
                searchItem,
                mainGrid
            ]
        });

        me.callParent(arguments);
    }
});