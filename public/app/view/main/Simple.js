Ext.define('Vmoss.view.main.Simple', {
    requires: [
        'Ext.ux.grid.FiltersFeature'
    ],
    extend: 'Vmoss.view.main.Basic',

    layout: 'fit',
    isSimple: true,

    buttonList: ['gridlargeradd'],

    featureKeys: ['add', 'modify', 'bench', 'grid'],
    extendKeys: ['fields'],

    initComponent:function () {
    	var me = this,
            modelName = (me.modelName || me.scaffold.split("-")[0]),
            featureList = me.featureCollect();

        featureList.gridFeature.push({
            xtype:'actioncolumn',
            menuDisabled: true,
            items: [{
                icon: 'extjs/examples/restful/images/delete.png',
                tooltip: 'Delete',
                handler: function(grid, rowIndex, colIndex) {
                    var model = grid.getStore().getAt(rowIndex);

                    Ext.Msg.confirm('提示信息', '确实要删除选中项吗?', function (confirm) {
                        if (confirm === 'yes') {
                            model.destroy();
                            grid.store.load();
                        }
                    })
                }
            }]
        });

        Ext.apply(me, {
            items:[
                Ext.create('Vmoss.view.main.Grid', {
                    instanceLabel: me.instanceLabel,
                    model: modelName,
                    featureList: featureList,
                    buttonList: me.buttonList,
                    gridConfig: me.girdConfig,
                    listeners: {
                        itemdblclick: function (view, instance) {
                            Ext.create('Vmoss.view.main.ModifyView', {
                                selInstance: instance,
                                instanceLabel: me.instanceLabel,
                                modelField: (featureList.modfiyFeature || featureList.benchFeature)
                            }).editVersion().show();
                        }
                    }
                })
            ]
        });

        me.callParent(arguments);
    }
});    