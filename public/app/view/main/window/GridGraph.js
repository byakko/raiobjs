Ext.define('Vmoss.view.main.window.GridGraph', {
    extend:'Ext.window.Window',

    resizable: false,
    modal: true,
    layout: 'fit',

    title:'销售量',
    height: 600,
    width: Ext.getDoc().dom.width * 0.9,

    initComponent:function () {
        var me = this,
            grid = me.grid,
            store = grid.store,
            options;

        options = {
            items:{
                xtype: 'chart',
                store: store,
                axes: [{
                    type: 'Numeric',
                    position: 'left',
                    fields: ['salesnumber'],
                    title: '销售量'
                }, {
                    type: 'Time',
                    position: 'bottom',
                    fields: ['work_dtm'],
                    title: '时间',
                    dateFormat: 'm/d'
                }],
                series: [{
                    type: 'line',
                    xField: 'work_dtm',
                    yField: 'salesnumber'
                }]
            }
        }

        Ext.apply(me, options);                    

        me.callParent(arguments);
    }
});