Ext.define('Vmoss.view.main.button.GridDirect', {
    extend:'Ext.button.Button',
    alias: 'widget.griddirect',

    text:'Graph',
    tooltipText: 'Graph',
    iconCls:"icon-export",

    handler:function () {
        var me = this,
            grid = me.grid;

        window.location = '#!/tab/UriageGraph-' + me.grid.store.storeId;
    },

    initComponent:function () {
        this.tooltip = this.tooltipText + this.instanceLabel,
        this.callParent(arguments);
    }
});