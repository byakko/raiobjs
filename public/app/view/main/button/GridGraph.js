Ext.define('Vmoss.view.main.button.GridGraph', {
    extend: 'Ext.button.Button',
    alias: 'widget.gridgraphbtn',

    text:'图表',
    iconCls:"icon-export",

    handler:function () {
        var grid = this.grid;

        Ext.create('Vmoss.view.main.window.GridGraph', {
            grid: grid
        }).show();
    }
});