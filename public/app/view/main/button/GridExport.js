Ext.define('Vmoss.view.main.button.GridExport', {
    extend:'Ext.button.Button',
    alias: 'widget.gridexport',

    text:'Export',
    tooltipText: 'Export',
    confirmText: 'Export?',
    iconCls:"icon-export",

    handler:function () {
        var me = this,
            grid = me.grid;

        Ext.Msg.confirm("提示!", me.confirmText, function (btn) {
            if (btn == "yes") {
                
            }
        });
    },

    initComponent:function () {
        this.tooltip = this.tooltipText + this.instanceLabel,
        this.callParent(arguments);
    }
});