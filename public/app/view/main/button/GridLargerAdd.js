Ext.define('Vmoss.view.main.button.GridLargerAdd', {
    extend: 'Vmoss.view.main.button.GridAdd',
    alias: 'widget.gridlargeradd',

    scale: 'large'
});