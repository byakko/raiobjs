Ext.define('Vmoss.view.main.major.FlowSample', {
    requires: [
        'Vmoss.model.major.FlowSample'
    ],
    extend:'Vmoss.view.main.Simple',

    title: '工作流演示',
    instanceLabel: '工作流',

    girdConfig: {
        panel: {
            stateful: true,
            stateId: 'flow-sample-grid',
            listeners: {
                itemdblclick: function (view, instance) {
                    window.location = '#!/tab/EntryForm-' + instance.get('id');
                }
            }
        }
    },

    fieldsExtend:[
        {field:'code', label:'编号'}
    ],

    gridFeature:[
        'code'
    ],

    benchFeature:[
        'code'
    ]
});