Ext.define('Vmoss.view.main.major.EntryForm', {
	requires: [
        'Vmoss.model.major.FlowSample'
    ],
    extend: 'Vmoss.view.main.FormFlow',

    title: '工作流',
    modelName: 'Vmoss.model.major.FlowSample',

    // flowItems: [
    // 	{
    // 		text: ''
    // 	}
    // ],

    itemGroup: [
    	'A',
    	'B'
    ]
});    