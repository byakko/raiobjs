Ext.define('Vmoss.view.main.major.CurrentGprsStorage', {
    extend:'Vmoss.view.main.Standard',

    title: 'GPRS自售机剩余库存信息',
    instanceLabel: '',
    buttonList: [],

    searchFeature:[
        'gprsterminal_cd',
        {
            field:'vm_id',
            association: 'Vmoss.model.major.CurrentGprsMachine',
            display:'vm_cd',
            xtype:'ccombo'
        }
    ],

    gridConfig: {
        store: {
            groupField: 'basyo_name'
        },
        panel: {
            features: [
                {
                    ftype: 'grouping',
                    groupHeaderTpl: '点位: {name}'
                }
            ]
        }
    },

    gridFeature:[
        'vm_cd',
        'column_no',
        'remained',
        'fullsyohinsu'
    ],

    benchFeature:[]
});