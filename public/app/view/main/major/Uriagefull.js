Ext.define('Vmoss.view.main.major.Uriagefull', {
    requires: [
        'Vmoss.model.major.Uriagefull'
    ],
    extend:'Vmoss.view.main.Simple',

    title: '全项服务作业信息管理',
    instanceLabel: '全项服务',

    buttonList: ['gridadd', 'gridgraphbtn'],

    girdConfig: {
        panel: {
            stateful: true,
            stateId: 'uriagefull-grid',
            features: [{
                ftype: 'filters',
                filters: [{
                    type: 'string',
                    dataIndex: 'denpyo_no'
                }, {
                    type: 'date',
                    dataIndex: 'work_dtm'
                }, {
                    type: 'string',
                    dataIndex: 'basyo_name'
                }, {
                    type: 'boolean',
                    dataIndex: 'patrol'
                }]
            }]
        }
    },

    fieldsExtend:[
        {field:'denpyo_no', label:'编号'},
        {field:'work_dtm', label:'时间'},
        {field:'salesnumber', label:'贩卖数'},
        {field:'amount', label:'收入'},
        {field:'patrol', label:'巡回'},
        {field:'basyo_name', label:'点位名称'}
    ],

    searchFeature:[
        'denpyo_no',
        'work_dtm'
    ],

    gridFeature:[
        {
            field: 'denpyo_no'
        },
        {
            field: 'work_dtm',
            xtype:'datecolumn',
            format:'Y/m/d H:i:s'
        },
        {
            field: 'basyo_name'
        },
        'salesnumber',
        'amount',
        'patrol'
    ],

    benchFeature:[
        'denpyo_no',
        'work_dtm'
    ]
});