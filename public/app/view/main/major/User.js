Ext.define('Vmoss.view.main.major.User', {
    extend:'Vmoss.view.main.Standard',

    title: '用户基础信息管理',
    instanceLabel: '用户',
    buttonList: ['gridexport'],

    gridConfig: {
        panel: {
            selType: 'cellmodel',
            plugins: [
                Ext.create('Ext.grid.plugin.CellEditing')
            ]
        }
    },

    searchFeature:[
        {field:'user_cd', vtype:'alphanum', xtype:'textfield', maxLength:4},
        {field:'user_name', xtype:'textfield', maxLength:30},
        {
            field:'bumon_id',
            association: 'Vmoss.model.major.Department',
            display:'bumon_mei',
            xtype:'ccombo'
        }
    ],

    gridFeature:[
        'user_cd',
        {
            field: 'user_name',
            width: 240,
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            field: 'department_name',
            width: 200,
            editor: {
                field:'bumon_id',
                association: 'Vmoss.model.major.Department',
                display:'bumon_mei',
                xtype:'ccombo'
            }
        },
        'birth_dtm',
        'nyusya_dtm',
        'tel_no',
        'email'
    ],

    benchFeature:[
        {field: 'user_cd', allowBlank: false},
        {field: 'user_name', allowBlank: false},
        {
            field:'bumon_id',
            association: 'Vmoss.model.major.Department',
            display:'bumon_mei',
            xtype:'ccombo'
        },
        'birth_dtm',
        'nyusya_dtm',
        'tel_no',
        'email'
    ]
});