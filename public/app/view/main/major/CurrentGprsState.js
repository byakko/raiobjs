Ext.define('Vmoss.view.main.major.CurrentGprsState', {
    extend:'Vmoss.view.main.Standard',

    title: 'GPRS自售机状态信息管理',
    instanceLabel: '',
    buttonList: [],

    searchFeature:[
        'gprsterminal_cd',
        'vm_cd'
    ],

    gridFeature:[
        'basyo_name',
        'state', 
        'vm_cd', 
        'trouble',
        'no_change',
        'lack_warning',
        'early_warning'
    ],

    benchFeature:[]
});