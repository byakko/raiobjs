Ext.define('Vmoss.view.main.FormFlow', {
    extend: 'Vmoss.view.main.Basic',

    layout:{
        type:'vbox',
        align:'stretch'
    },

    initComponent: function () {
        var me = this,
        	params = me.scaffold.split("-"),
        	modelName = (me.modelName || params[0]),
        	modelId = params[1],
        	flowIndex = 0,
            panel = Ext.create('Ext.panel.Panel', {
                frame: true,
                tbar: [
                    {
                        text: '后退',
                        handler: function () {
                            me.changeState(-1);
                        }
                    },
                    '->',
                    {
                        text: '前进',
                        handler: function () {
                            me.changeState(1);
                        }
                    }
                ],
                flex: 1,
                layout: 'fit'
            });

        Ext.apply(me, {
            flowIndex: flowIndex,
        	items: [
	    		// Ext.create('Vmoss.view.main.nav.Flow', {
       //  			flowItems: me.flowItems,
       //  			flowIndex: flowIndex
       //  		}),
        		panel
        	]
        });

        Ext.ModelManager.getModel(modelName).load(modelId, {
            success: function (instance) {
                me.formsStruct(instance, panel);
            }
        });

		me.callParent(arguments);
    },

    changeState: function (offset) {
        var me = this,
            forms = me.items.items[0].items.items;

        if (me.formsReady) {
            if (me.flowIndex + offset >= 0 && me.flowIndex + offset <= forms.length - 1) {

                // forms[me.flowIndex].submit();
                forms[me.flowIndex].hide();

                me.flowIndex += offset;
                forms[me.flowIndex].show();
            }
        }
    },

    formsStruct: function (instance, panel) {
        var me = this,
            unitGroup;

        // for (item in me.flowItems) {
        //    flowForms.unshift(Ext.create(item.view, {
        //        instance: instance
        //    }));
        // }

        // instance.
        instance.getStruct(
            {
                callback: function(struct, operation) {
                    Ext.each(me.itemGroup, function (group) {
                        unitGroup = [];
                        Ext.each(struct.get('source').items, function (unit) {
                            if (unit.group === group) {
                                unitGroup.push(unit);
                            }
                        });
                        panel.add(me.formBuild(unitGroup));
                    });                   

                    me.formsReady = true;
                    panel.items.items[0].show();
                }
            }
        );
    },

    formBuild: function (units) {
        return Ext.create('Ext.form.Panel', {
            items: units,
            frame: true,
            layout: 'anchor',

            bodyPadding: 5,
            hidden: true,

            defaults: {
                anchor: '50%'
            },

            defaultType: 'textfield'
        });
    }
});