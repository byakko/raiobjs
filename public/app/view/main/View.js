Ext.define('Vmoss.view.main.View', {
    requires: [
        'Vmoss.view.main.Welcome',
        'Vmoss.view.main.Standard',
        'Vmoss.view.main.Simple',
        'Vmoss.view.main.FormFlow'
    ],
    extend:'Ext.tab.Panel',
    alias:'widget.mainview',

    margins: '5 5 5 5',
    region: 'center',

    listeners: {
        tabchange: function (tabPanel, newTab, oldTab, options) {
            if (newTab.scaffold) {
                window.location = '#!/tab/' + newTab.scaffold;
                return false;
            }
            else {
                window.location = '#';
            }
        }
    },

    initComponent: function () {
        this.items = [
            Ext.create('Vmoss.view.main.Welcome')
        ];

        this.callParent(arguments);
    },

    openPanel: function (scaffold) {
        var me = this,
            scaffold = (scaffold || ""),
            panel,
            panelName = 'Vmoss.view.main.major.' + scaffold.split("-")[0],
            item = me.items.items || [];

        Ext.each(item, function(tab){
            if(tab.scaffold && tab.scaffold === scaffold){
                panel = tab;
                return false;
            }
        });

        if (!panel) {
            panel = Ext.create(panelName, {
                scaffold: scaffold
            });
            me.add(panel);
        }

        me.setActiveTab(panel);
    }
});