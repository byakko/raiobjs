Ext.define('Vmoss.view.main.Basic', {
	extend: 'Addition.lib.CPanel',

	isMainBasic: true,

	frame: true,
    closable: true,

    // 与model相关的特征收集, 命名规则为xxxFeature, xxxExtend上的属性将会被合并到feature之上
    featureCollect: function () {
        var me = this,
            collection = {},
            featureKeys = me.featureKeys || [],
            extendKeys = me.extendKeys || [];

        Ext.Array.each(featureKeys, function (featureKey) {
            if (me[featureKey + 'Feature']) {
                collection[featureKey + 'Feature'] = [];
                Ext.each(me[featureKey + 'Feature'], function (featureUnit) {
                    if (typeof(featureUnit) === 'string') {
                        featureUnit = {
                            field: featureUnit
                        };
                    }
                    collection[featureKey + 'Feature'].unshift(featureUnit);
                });
                Ext.each(extendKeys, function (extendKey) {
                    collection[featureKey + 'Feature'] = Addition.Tool.objArrayMerge(collection[featureKey + 'Feature'], me[extendKey + 'Extend'], ['field']);
                })
            }
        });

        return collection;
    }
});