﻿Ext.define('Vmoss.view.header.AlterpwForm', {
    requires: [
        'Ext.layout.container.Form'
    ],
	extend: 'Addition.lib.CForm',
    alias: 'widget.alterpwform',

    okText: 'Ok',
    cancelText: 'Cancel',
    notEqual: 'Not Equal',
    successTitle: 'Success',
    successText: 'Password Modify Successfully',

    fireUpdate: 'before',

    frame:true,
    layout:'form',
    width:300,
    bodyPadding: 5,
    buttonAlign:"center",

    fieldDefaults: {
        xtype: 'textfield',
        msgTarget: 'side',
        autoFitErrors: false
    },

	modelField: [
        {
            field: 'password',
            label: 'Password', 
            inputType: 'password', 
            allowBlank: false
        },
        {
            field: 'alterPassword',
            label: 'Modify Password', 
            itemId: 'alterPassword', 
            inputType: 'password', 
            allowBlank: false
        },
        {
            field: 'confirmPassword', 
            label: 'Confirm Password', 
            inputType: 'password', 
            allowBlank: false,  
            vtype: 'password', 
            initialPassField: 'alterPassword'
        }
    ],

    initComponent:function () {
        var me = this;

        //添加vtype——password,验证修改密码与确认密码要一致
        Ext.apply(Ext.form.field.VTypes, {
            password: function(val, field) {
                if (field.initialPassField) {
                    var pwd = field.up('form').down('#' + field.initialPassField);
                    return (val == pwd.getValue());
                }
                return false;
            },
            passwordText: me.notEqual
        });

        Ext.apply(me, {
            buttons:[
                { 
                    xtype: "button", 
                    text: me.okText, 
                    scope: me,
                    handler:function () {
                        if (me.getForm().isValid()){
                            me.bind.save({
                                params: me.getValues(),
                                success: function (response, options) {
                                    var obj = Ext.decode(response.responseText);
                                    if (obj.success){
                                        me.up('alterpwwidget').hide();
                                        Ext.Msg.alert(me.successTitle, me.successText);
                                    } else {
                                        me.getForm().markInvalid(obj.errors);
                                    }
                                }
                            })
                        }
                    }
                },
                { 
                    xtype: "button", 
                    text: me.cancelText, 
                    scope: me,
                    handler:function () {
                        me.up('alterpwwidget').hide();
                    }
                }
            ]
        });

        me.callParent(arguments);
    }

});