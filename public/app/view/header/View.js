﻿Ext.define('Vmoss.view.header.View', {
    requires: [
        'Vmoss.view.header.UserWidget'
    ],
    extend:'Addition.lib.CPanel',
    alias:'widget.headerview',

    region:"north",
    height:55,
    cls:'loginbgimage',
    baseCls:'my-panel-no-border',

    initComponent:function () {
        Ext.applyIf(this, {
            html:"<img src='/resources/images/SampleLogo.png'>",
            rbar: Ext.create('Vmoss.view.header.UserWidget')
        });

        this.callParent(arguments);
    }
});