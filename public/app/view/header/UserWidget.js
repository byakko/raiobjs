﻿Ext.define('Vmoss.view.header.UserWidget', {
    requires: [
        'Vmoss.model.CurrentUser',
        'Vmoss.view.header.UserMenu',
        'Vmoss.view.header.AlterpwView'
    ],
    extend:'Ext.toolbar.Toolbar',
    alias: 'widget.userwidget',

    cls:'loginbgimage',
    baseCls:'my-panel-no-border',

    layout: {
        type: 'hbox',
        padding:'0, 50, 0, 0',
        pack:'end',
        align:'middle'
    },

    initComponent:function () {
		var me = this,
            currentUser = Vmoss.model.CurrentUser.load();

        Ext.applyIf(me, {
			items: [
                Ext.create('Vmoss.view.header.UserMenu', {
                    currentUser: currentUser,
                    alterView: Ext.create('Vmoss.view.header.AlterpwView', {
                        currentUser: currentUser
                    })
                })
            ]
        });

        me.currentUser = currentUser;

        this.callParent(arguments);
    }
});
