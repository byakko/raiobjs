Ext.define('Vmoss.view.header.UserMenu', {
    extend:'Ext.button.Button',
    scale: 'large',
    cls: 'larger-white-font',

    modifyPwd: 'Modfiy PassWord',
    logout: 'Logout',
	
    initComponent:function () {
		var me = this;

		Ext.apply(this, {
            menuAlign: 'tr-br',
			menu: {
				items: [
					{
						text: me.modifyPwd,
                        handler: me.alterpw,
                        scope: me
					}, {
						text: me.logout,
						handler: function() {
							window.location.href = '/admin/logout'
						}
					}
				]
			}
        });

        me.currentUser.bindProperty({
            component: me,
            field: 'name',
            handle:{
                set: me.setText,
                get: me.getText
            }
        });

		me.callParent(arguments);
    },

	alterpw: function(){
		this.alterView.show();
	} 
});