﻿Ext.define('Vmoss.view.header.AlterpwView', {
    requires: [
        'Vmoss.view.header.AlterpwForm'
    ],
    extend:'Ext.window.Window',
    alias:'widget.alterpwwidget',

    closeAction: 'hide',
    title: "Modify PassWord",
    resizable:false,
    modal:true,
    layout: 'fit',

    initComponent:function () {
        var me = this;

        Ext.applyIf(this, {
            items: [
                Ext.create('Vmoss.view.header.AlterpwForm', {
                    bind: me.currentUser
                })
            ]
        });

        this.callParent(arguments);
    }
});