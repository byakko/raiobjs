//Todo: 此处的请求完全没有进入封装, 一个裸露的请求和未经处理的数据直接使用
Ext.define('Vmoss.view.menu.View', {
    requires: [
        'Vmoss.view.menu.Tree',
        'Vmoss.store.TreeStore',
        'Ext.layout.container.Accordion'
    ],
    extend: 'Addition.lib.CPanel',
    alias:'widget.menuview',

    stateful: true,
    stateId: 'menu-view',

    title: 'Menu',
    region: 'west',
    margins: '5 0 5 5',
    split:true,
    layout:'accordion',
    collapsible: true,
    collapsed: true,
    width:240,

    initComponent:function () {
        var me = this;

        Ext.Ajax.request({
            method:'GET',
            url:'/services/main/actions/menu/invoke',
            success:function (response, options) {
                var obj = Ext.decode(response.responseText);
                Ext.Array.each(obj.root, function (config) {
                    var tree = Ext.create('Vmoss.view.menu.Tree', {
                        title:config.title,
                        store:Ext.create('Vmoss.store.TreeStore', config.TreeStore)
                    });
                    tree.on({
                        itemclick: me.mainBuilder,
                        scope: me
                    });
                    me.add(tree);
                });
            }
        });

        me.callParent(arguments);
    },

    mainBuilder:function(tree, instance){
        if (instance.data.leaf){
            window.location = '#!/tab/' + instance.raw.model;
        }
        else {
            if(instance.isExpanded()){
                instance.collapse();
            }
            else {
                instance.expand();
            }
        }
    }

});