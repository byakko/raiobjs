Ext.define('Vmoss.view.menu.Tree', {
    extend: 'Ext.tree.Panel',

    // autoScroll:true,
    frame:false,
    border:false,
    rootVisible:false,
    width:120,

    initComponent: function(options) {
        this.callParent(arguments);
    }
});