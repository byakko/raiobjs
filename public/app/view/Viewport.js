Ext.define('Vmoss.view.Viewport', {
    requires: [
        'Addition.tool.Base',
        'Vmoss.view.header.View',
        'Vmoss.view.menu.View',
        'Vmoss.view.main.View',
        'Ext.layout.container.Border'
    ],
    extend:'Ext.container.Viewport',
    
    layout: "border",

    initComponent:function () {
        Ext.tip.QuickTipManager.init();
        
        this.items = [
            Ext.create('Vmoss.view.header.View'),
            Ext.create('Vmoss.view.menu.View'),
            Ext.create('Vmoss.view.main.View')
        ];

        this.callParent(arguments);
    }
});