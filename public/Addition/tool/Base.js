Ext.define('Addition.tool.Base', {
    singleton:true,
    alternateClassName: 'Addition.Tool',

    requires:[
        // 'Addition.lib.*'
//        'Addition.lib.CForm',
//        'Addition.lib.CCombo',
//        'Addition.lib.CModel',
//        'Addition.lib.CStore',
//        'Addition.lib.CPanel'
    ],

    logEnable: false,
    requestLog: true,
    lang: 'EN',

    _cacheMe: {},

    functionMerge: function () {
        // 将以参数形式传入的函数句柄转存为数组
        var fnc_list = [].slice.call(arguments, 0);
        // 清理未定义的参数
        Ext.Array.unique(fnc_list);
        Ext.Array.remove(fnc_list, undefined);
        return function () {
            for (var i = 0, length = fnc_list.length; i < length; i++) {
                // 实际参数转化为数组
                var params = [].slice.call(arguments, 0);
                fnc_list[i].apply(this, params)
            }
        }
    },

    log: function (obj) {
        if (this.logEnable) {
            console.log(obj);
        }
    },

    //比较两个对象是否含有相同的值, 第三个参数为迭代器. 当遇到与迭代器内相同的属性时, 会对该属性的值作迭代比较, 而迭代器属性对应的值则作为下一层比较的迭代器
    eqlObject:function (obj, anotherObj, iterate) {
        if (Ext.Object.getSize(obj) != Ext.Object.getSize(anotherObj)) {
            return false;
        }
        var warden = true;
        iterate = iterate || {};
        for (var attr in obj) {
            if (obj[attr] != anotherObj[attr]) {
                if (!(iterate[attr] && this.eqlObject(obj[attr], anotherObj[attr], iterate[attr]))) {
                    warden = false;
                    break;
                }
            }
        }
        return warden
    },

    // 合并两个obj数组, 只支持单层合并, 以linkAttr为依据寻找相同的对象, 放弃合并原数组中无法找到的对象
    objArrayMerge: function (targetArray, mergeArray, linkAttr) {
        var mergeIndex,
            targetIndex,
            newUnit,
            attributeIndex,
            warden,
            result = [];

        for (targetIndex in targetArray) {
            newUnit = Ext.merge({}, targetArray[targetIndex]);
            result.unshift(newUnit);
            for (mergeIndex in mergeArray) {
                warden = true;
                for (attributeIndex in linkAttr) {
                    if (targetArray[targetIndex][linkAttr[attributeIndex]] !== mergeArray[mergeIndex][linkAttr[attributeIndex]]) {
                        warden = false;
                        break;
                    }
                }  
                if (warden) {
                    Ext.merge(newUnit, mergeArray[mergeIndex]);
                }
            }
        }
        
        return result;
    },

    //将游离在对象上的feature数组收集到一个Object上 
    //此方法在merge数组时无法正确达到目的, 并且与程序本身耦合, 并且model已经不赞成采用此方法
    featureCollect:function (object, config) {
        config = config || {};
        var collection = object.featureList || {},
            lunchScan = config.scan || false,
            featureKeys = [
                'add', 'modify', 'bench', 'search', 'grid', 'field'
            ];

        if (lunchScan) {
            Ext.Array.each(featureKeys, function (featureKey) {
                if (object[featureKey + 'Feature']) {
                    collection[featureKey + 'Feature'] = Ext.merge(object[featureKey + 'Feature'], collection[featureKey + 'Feature']);
                }
            });
        }

        return collection;
    },

    errorMessage:function (message, config) {
        Ext.Msg.show({
//            title:'错误信息',
            msg:message,
            buttons:Ext.MessageBox.OK,
            icon:Ext.MessageBox.ERROR
        });
    },

    // 返回一个仅复制原对象直接属性的对象
    copy:function(obj){
        return Ext.merge({}, obj);
    },

    // 添加一个方法确保当传入arguments时, arguments的第一个参数为一个可用对象({})
    insureArg:function(firstArg, args){
        if(!firstArg) {
            firstArg = {};
            args[0] = firstArg;
            args.length = 1;
        }
        return firstArg;
    },

    promptBox: function(){
        var msgCt;

        function createBox(t, s){
            return '<div class="message"><h3>' + t + '</h3><p>' + s + '</p></div>';
        }

        return function(title, format){
            if(!msgCt){
                msgCt = Ext.DomHelper.insertFirst(document.body, {id:'prompt-box-div'}, true);
            }
            var format = (format === undefined ? '' : format),
                s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1)),
                m = Ext.DomHelper.append(msgCt, createBox(title, s), true);
            m.hide();
            m.slideIn('t').ghost("t", { delay: 1500, remove: true});
        };
    }(),

    appTranslate: function (source) {
        var me = this,
            componentName;

        for (componentName in source) {
            me.cmpTranslate(componentName, source[componentName]);
        }
    },

    cmpTranslate: function (componentName, propertys) {
        var me = this,
            scopes = componentName.split('.'),
            localeCmpName = [
                scopes.shift(),
                'locale',
                me.lang,
                scopes.join('.')
            ].join('.');

        Ext.define(localeCmpName, Ext.merge({
            override: componentName
        }), function () {
            var me = this,
                cmpPrototype = me.prototype;

            if (cmpPrototype.launchTranslate) {
                propertys = cmpPrototype.launchTranslate(propertys);
                if (!propertys) {
                    return;
                }
            }    

            // 此处手写了prototype的覆盖机制
            Ext.merge(cmpPrototype, propertys);
        });
    },

    csrfToken:function () {
        var result = {},
            metas = Ext.dom.Query.select('meta'),
            param,
            token;

        Ext.each(Ext.dom.Query.select('meta'), function (meta) {
            switch(meta.name){
                case 'csrf-param':
                    param = meta.content;
                    break;
                case 'csrf-token':
                    token = meta.content;
                    break;
            }
        });

        result[param] = token;
        return result;
    }(),

    // 提供一组方法缓存对象(方便供Chrome的Console内调用, 同ID对象会被覆盖)
    cacheById: function(cache){
        this._cacheMe[cache.id] = cache;
        console.log(cache.id + ' Cached');
    },

    getCache: function(id){
        return this._cacheMe[id];
    },

    cacheClean: function(){
        this._cacheMe = {};
    }
});