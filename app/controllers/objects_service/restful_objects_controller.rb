module ObjectsService
  class RestfulObjectsController < ApplicationController
    def restful_class
      unless @restful_class
        @restful_class = params[:restful_class].classify.constantize
        raise 'This Class is not Restful Object.' if !@restful_class.include?(RestO::Model)
      end
      @restful_class
    end

    def index
      options = {
          :params => params
      }
      options[:sort_params] = JSON.parse(params[:sort], :symbolize_names => true) if params[:sort] && params[:sort].is_a?(String)
      options[:filter_params] = JSON.parse(params[:filter], :symbolize_names => true) if params[:filter] && params[:filter].is_a?(String)
      render extjs_struct(restful_class.query(options))
    end

    def show
      render extjs_struct(restful_class.find(params[:id]).mapping_exec(:greedy => true))
    end

    def create
      restful_class.new.mapping_attr({
          :params => params,
          :greedy => true
      }).save!
      render extjs_struct
    end

    def update
      restful_class.find(params[:id]).mapping_attr({
          :params => params,
          :greedy => true
      }).save!
      render extjs_struct
    end

    def destroy
      restful_class.find(params[:id]).destroy
      render extjs_struct
    end

    def action_invoke
      render text: "doing"
    end
  end
end