# encoding: utf-8
class Uriagefull < ActiveRecord::Base
	belongs_to :basyo
	belongs_to :sagyotanto, :class_name => 'User'
	belongs_to :sagyosousa, :class_name => 'User'

	has_one :vm, :through => :basyo

	has_many :uriagefullcols, :dependent => :destroy
	has_many :vmcolumns, :through => :uriagefullcols
	has_many :kaisyukbnuricols, :dependent => :destroy

	def salesnumber
	    kaisyukbnuricols.inject(0) { |hanbaisu, kaisyukbnuricol|
	        hanbaisu + kaisyukbnuricol.actualhanbaisu.to_i
	    }
	end

	def amount
	    kaisyukbnuricols.inject(0) { |amount, kaisyukbnuricol|
	        (kaisyukbnuricol.kind.kindcategory_cd.to_i == 33 || kaisyukbnuricol.kind.kindcategory_cd.to_i == 34 || kaisyukbnuricol.kind.kindcategory_cd.to_i == 35) ? 	amount + kaisyukbnuricol.actualprice.to_i * kaisyukbnuricol.actualhanbaisu.to_i : amount
	    }
	end

	def patrol?
		total = self.kaisyuwk_kbn.to_i +
		    self.hojuwk_kbn.to_i +
		    self.colchgwk_kbn.to_i +
		    self.tanawk_kbn.to_i +
		    self.clrcntwk_kbn.to_i +
		    self.syuriwk_kbn.to_i +
		    self.eigyowk_kbn.to_i +
		    self.sonotawk_kbn.to_i
		total > 0
	end

	def previous_uriagefull
		self.class.joins(:basyo).where(["basyos.customer_id = :customer_id AND basyos.basyo_cd = :basyo_cd", {:customer_id => self.basyo.customer_id, :basyo_cd => self.basyo.basyo_cd}]).where(["work_dtm < :work_dtm", {:work_dtm => self.work_dtm}]).order(" work_dtm asc").last
	end

	def next_uriagefull
		self.class.joins(:basyo).where(["basyos.customer_id = :customer_id AND basyos.basyo_cd = :basyo_cd", {:customer_id => self.basyo.customer_id, :basyo_cd => self.basyo.basyo_cd}]).where(["work_dtm > :work_dtm", {:work_dtm => self.work_dtm}]).order(" work_dtm asc").first
	end
end