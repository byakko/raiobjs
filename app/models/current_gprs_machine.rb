class CurrentGprsMachine < GprsMachine
	default_scope do
		where('`vmgprsterminalinfos`.`rireki_kaisi_dtm` <= :current_time AND `vmgprsterminalinfos`.`rireki_syuryo_dtm` >= :current_time', current_time: Time.now.strftime('%Y%m%d%H%M%S'))
	end

	self.mapping = {
      :id => :persist,
      :vm_cd => :persist,
      :query => {
          :query => {
              :field => "vm_cd",
              :seek_by => :similar
          }
      }
  }
end