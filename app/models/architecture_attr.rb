# encoding: utf-8
class ArchitectureAttr < ActiveRecord::Base
	belongs_to :architecture_node, foreign_key: :node_id

	def to_struct
		case self.value_type
		when 1
			self.value.to_i
		when 3
			self.value == 1
		when 4
			ArchitectureNode.find(self.value.to_i).to_struct
		else
			self.value
		end
	end
end