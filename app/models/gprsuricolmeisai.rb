# encoding: utf-8
class Gprsuricolmeisai < ActiveRecord::Base
  belongs_to :uricolinfo, :class_name => 'Gprsuricolinfo', :primary_key => "id"
  has_one :gukind, :class_name => 'Gprskind', :primary_key => "kaisyukbn", :foreign_key => "gprskind_cd",:conditions =>  "gprskinds.kindcategory_cd in (33,34,35)"
  has_one :gprskind, :class_name => 'Gprskind', :primary_key => "kaisyukbn", :foreign_key => "gprskind_cd",:conditions =>  "gprskinds.kindcategory_cd in (33,34,35)"

  def gprsvmcolpriceinfo
    Gprsvmcolpriceinfo.where(:kaisyukbn => self.kaisyukbn, :gprsuricol_id => self.gprsuricol_id).first
  end
end
