# encoding: utf-8
class Gprsuricolinfo < ActiveRecord::Base
  has_many :gprsuricolmeisais, :foreign_key => 'gprsuricol_id'
  has_many :uricolmeisais   ,:class_name => 'Gprsuricolmeisai', :primary_key => 'id', :foreign_key => 'gprsuricol_id'
  has_many :vmcolpriceinfos ,:class_name => 'Gprsvmcolpriceinfo',:primary_key => 'id', :foreign_key => 'gprsuricol_id'
  has_many :gprsvmcolpriceinfos ,:class_name => 'Gprsvmcolpriceinfo',:primary_key => 'id', :foreign_key => 'gprsuricol_id'
  belongs_to :vmgprsterminalinfo,:primary_key => "gprsterminal_cd" ,:foreign_key => "gprsterminal_cd"
end
