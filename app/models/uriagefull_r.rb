# encoding: utf-8
class UriagefullR < Uriagefull
  include RestO::Model

  default_scope includes({kaisyukbnuricols: :kind}, :basyo)

  self.mapping = {
      id: :persist,
      denpyo_no: :persist,
      work_dtm: :persist,
      salesnumber: {
          get: 'salesnumber'
      },
      basyo_name: {
        get: 'basyo.basyo_name',
        query: {
          seek_by: :similar,
          field: '`basyos`.`basyo_name`'
        }
      },
      amount: {
        get: 'amount'
      },
      patrol: {
        get: 'patrol?'
      }
  } 
end