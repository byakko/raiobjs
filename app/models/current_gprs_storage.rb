class CurrentGprsStorage < Vmcolumn
  include RestO::Model
  self.mapping = {
      vm_id: {
          :query => {
              :method => :find_with_vmgprsterminalinfo
          },
      },
      gprsterminal_cd: {
          query: {
              :method => :find_with_vmgprsterminalinfo
          }
      },
      remained: {
          get: {
              method: :get_remained
          }
      },
      column_no: {
          get: :column_no
      },
      id: {
          get: :id
      },
      basyo_name: {
          get: 'basyo.basyo_name'
      },
      vm_cd: {
          get: 'basyo.vm.vm_cd'
      },
      fullsyohinsu: {
          get: :fullsyohinsu
      }
  }

  def get_remained(options)
    vmgprsterminalinfo = (options[:vmgprsterminalinfo]) ? options[:vmgprsterminalinfo] : Vmgprsterminalinfo.where('rireki_kaisi_dtm <= :current_time AND rireki_syuryo_dtm >= :current_time', current_time: options[:current_time].strftime('%Y%m%d%H%M%S')).where(vm_id: self.basyo.vm.id).first 
    return 0 if vmgprsterminalinfo.nil?

    vmgprsterminalinfo.storage_remained({
        basyo: self.basyo,
        current_time: options[:current_time],
        vmcolumns: [self]
    })[0][:remained]
  end

  def self.find_with_vmgprsterminalinfo(condition, options)
    return false unless options[:vmgprsterminalinfo]
    condition.where('vm_id = :vm_id', vm_id: options[:vmgprsterminalinfo].vm_id)
  end

  def self.query(options)
    current_time = Time.now
    options[:current_time] = current_time

    if !options[:params][:vm_id].blank? || !options[:params][:gprsterminal_cd].blank?
      info_condition = Vmgprsterminalinfo.where('rireki_kaisi_dtm <= :current_time AND rireki_syuryo_dtm >= :current_time', current_time: current_time.strftime('%Y%m%d%H%M%S'))

      info_condition = info_condition.where(gprsterminal_cd: options[:params][:gprsterminal_cd]) unless options[:params][:gprsterminal_cd].blank?
      info_condition = info_condition.where(vm_id: options[:params][:vm_id]) unless options[:params][:vm_id].blank?

      vmgprsterminalinfo = info_condition.first
      return mapping.dump_empty if vmgprsterminalinfo.nil?
      options[:vmgprsterminalinfo] = vmgprsterminalinfo
    end

    gmachine_list = CurrentGprsMachine.select('`vms`.`id`').all.collect { |gmachine|
      gmachine.id
    }

    condition = self.joins(:basyo).includes(:basyo).where('rireki_kaisi_dtm <= :current_time AND rireki_syuryo_dtm >= :current_time AND vm_id IN (:gmachine_list)', current_time: current_time.strftime('%Y%m%d%H%M%S'), gmachine_list: gmachine_list)

    condition = mapping.add_condition(options.merge(condition_struct: condition))
    condition = mapping.add_sort(options.merge(condition_struct: condition))

    mapping.struct_exec(condition, options)

    # 另一种写法
    # condition = mapping.add_condition(options).joins(:basyo).where('receive_dtm <= :current_time', current_time: current_time.strftime('%Y%m%d%H%M%S'))

    # column_no_list = condition.select("column_no").uniq.all

    # source = column_no_list.inject([]) { |result, column_no_unit|
    #   result << condition.where(column_no: column_no_unit.column_no).order('receive_dtm ASC').last
    # }
    # mapping.struct_array(source, options)
  end
end