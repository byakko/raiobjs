# encoding: utf-8
class CurrentGprsState < CurrentGprsMachine
  include RestO::Model
  default_scope do
    includes(:vmgprsterminalinfos, :basyos).joins(:basyos).where('`basyos`.`rireki_kaisi_dtm` <= :current_time AND `basyos`.`rireki_syuryo_dtm` >= :current_time', current_time: Time.now.strftime('%Y%m%d%H%M%S'))
  end  

	self.mapping = {
      id: :persist,
      vm_cd: :persist,
      gprsterminal_cd: {
          query: {
              :field => '`vmgprsterminalinfos`.`gprsterminal_cd`'
          }
      },
      basyo_name: {
          get: 'basyos[0].basyo_name'
      },
      trouble: {
          get: :get_trouble
      },
      no_change: {
          get: :get_no_change
      },
      lack_warning: {
          get: :lack_warning
      },
      early_warning: {
          get: :early_warning
      },
      state: {
          get: :get_state
      }
  }

  def get_trouble
    return nil if self.vmgprsterminalinfos.empty?

    vmtroubleinfo = VwVmGprsjoutaijouhou.where(gprsterminal_cd: self.vmgprsterminalinfos[0].gprsterminal_cd, state: 3).order('receive_dtm ASC').last
    return nil if vmtroubleinfo.nil?

    info = vmtroubleinfo.info.to_s
    return nil if info[2, 4] == '0000'

    trouble_message = []
    2.upto(info.length / 4 + 1) { |index|
      trouble_message << '第' + (index - 1).to_s + '条错误代码: ' + info[index, 4]
    }
    unless trouble_message.empty?
      @exception = true
      trouble_message.join('; ')
    end
  end

  def get_no_change
    return nil if self.vmgprsterminalinfos.empty?

    no_change_info = VwVmGprsjoutaijouhou.where(gprsterminal_cd: self.vmgprsterminalinfos[0].gprsterminal_cd, state: 6).order('receive_dtm ASC').last

    nil
  end

  def lack_warning
    return nil if self.vmgprsterminalinfos.empty?
    return nil if self.basyos.empty?

    warning_info = VwVmGprsjoutaijouhou.where(gprsterminal_cd: self.vmgprsterminalinfos[0].gprsterminal_cd, state: 5).order('receive_dtm ASC').last

    return nil if warning_info.nil?
    @warning_columns = []

    warning_message = []
    basyos[0].vmcolumns.collect { |column|
      if warning_info.info[column.column_no - 1] == '1'
        warning_message << '第' + column.column_no.to_s + '货道售空'
        @warning_columns << column.column_no
      end
    }
    unless warning_message.empty?
      @exception = true
      warning_message.join('; ')
    end
  end

  def early_warning
    return nil if self.vmgprsterminalinfos.empty?
    return nil if self.basyos.empty?

    early_warning_message = []

    vmgprsterminalinfos[0].storage_remained({
      basyo: self.basyos[0],
      current_time: Time.now
    }).each { |unit|
      if unit[:remained] < unit[:vmcolumn].fullsyohinsu.to_i / 3
        if !@warning_columns || !@warning_columns.any? { |column_no|
          column_no == unit[:vmcolumn].column_no
        }
          early_warning_message << '第' + unit[:vmcolumn].column_no.to_s + '货道剩余' + unit[:remained].to_s + '件商品'
        end
      end
    }
    unless early_warning_message.empty?
      @exception = true
      early_warning_message.join('; ')
    end
  end

  def get_state
    @exception ? '异常' : '正常'
  end
end