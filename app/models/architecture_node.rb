# encoding: utf-8
class ArchitectureNode < ActiveRecord::Base
  include RestO::Model
	has_many :architecture_attrs, foreign_key: :node_id

	self.mapping = {
		id: :persist,
		source: {
		  	get: :get_source
		}
	}

	def to_struct
		struct = {}

		architecture_attrs.group_by { |attr|
			attr.name
		}.each { |field_name, attrs|  
			if attrs.any? { |attr| 
				attr.single == 1
			}
				struct[field_name.to_sym] = attrs.first.to_struct
			else
				struct[field_name.to_sym] = attrs.collect { |attr|
					attr.to_struct
				}
			end
		}

    	struct
	end

	def get_source
		to_struct.to_json
	end
end