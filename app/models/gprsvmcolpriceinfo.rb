# encoding: utf-8
class Gprsvmcolpriceinfo < ActiveRecord::Base
  belongs_to :uricolinfo, :class_name => 'Gprsuricolinfo', :primary_key => "id"
  belongs_to :gukind, :class_name => 'Gprskind', :primary_key => "gprskind_cd", :foreign_key => "kaisyukbn",:conditions =>  "gprskinds.kindcategory_cd in (33,34,35)"
end
