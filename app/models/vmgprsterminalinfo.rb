class Vmgprsterminalinfo < ActiveRecord::Base
  belongs_to :vm

  def storage_remained(options)
    current_time = (options[:current_time] || Time.now)
    vmcolumns = (options[:vmcolumns] || options[:basyo].vmcolumns)

    vmcolumns.inject([]) { |collection, vmcolumn|
      unit = {
          vmcolumn: vmcolumn,
          remained: 0
      }

      collection << unit

      gprsuricolinfo = Gprsuricolinfo.includes(:gprsuricolmeisais).where('`gprsuricolmeisais`.kaisyukbn = \'3301\'').where(gprsterminal_cd: self.gprsterminal_cd, column_no: vmcolumn.column_no).order('`gprsuricolinfos`.`receive_dtm` ASC').where('`gprsuricolinfos`.`receive_dtm` <= :current_time', current_time: current_time.strftime('%Y%m%d%H%M%S%L')).last
      next collection if gprsuricolinfo.nil?

      last_uriage = Uriagefull.joins(:basyo).where(["basyos.customer_id = :customer_id AND basyos.basyo_cd = :basyo_cd", {:customer_id => options[:basyo].customer_id, :basyo_cd => options[:basyo].basyo_cd}]).where(["work_dtm < :work_dtm", {:work_dtm => gprsuricolinfo.receive_dtm}]).order(" work_dtm asc").last
      next collection if last_uriage.nil?

      last_uri_col = Uriagefullcol.where(uriagefull_id: last_uriage.id, vmcolumn_id: vmcolumn.id).first
      next collection if last_uri_col.nil?
      last_kaisyu = Kaisyukbnuricol.joins(:kind).where('kindcategory_cd = \'33\' AND kind_cd = \'01\'').where(uriagefull_id: last_uriage.id, vmcolumn_id: vmcolumn.id).first
      next collection if last_kaisyu.nil?

      unit[:remained] = last_uri_col.konkaizaikosu.to_i + last_kaisyu.konkaihanbaisu.to_i - gprsuricolinfo.gprsuricolmeisais[0].hanbaisu.to_i

      collection
    }
  end
end