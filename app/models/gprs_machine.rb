class GprsMachine < Vm
  include RestO::Model
	default_scope do
    # where('`vms`.`id` IN(SELECT vm_id FROM vmgprsterminalinfos)')
    # IN(EXIST)的用GROUP替换写法
    joins(:vmgprsterminalinfos).group('`vms`.`id`')
	end

	self.mapping = {
      :id => :persist,
      :vm_cd => :persist,
      :query => {
          :query => {
              :field => "vm_cd",
              :seek_by => :similar
          }
      }
  }
end